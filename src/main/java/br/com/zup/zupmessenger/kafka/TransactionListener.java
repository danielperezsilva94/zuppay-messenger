package br.com.zup.zupmessenger.kafka;

import br.com.zup.zupmessenger.model.Transaction;
import br.com.zup.zupmessenger.service.EmailSenderService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@RequiredArgsConstructor
public class TransactionListener {
    private final String TOPIC = "processed-transactions";
    private final ObjectMapper mapper;

    @Autowired
    EmailSenderService emailSender;

    @KafkaListener(topics = TOPIC)
    public void transactionReader(String kafkaMessage){
        try {
            emailSender.sendEmail(mapper.readValue(kafkaMessage, Transaction.class));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
