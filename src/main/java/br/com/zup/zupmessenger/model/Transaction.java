package br.com.zup.zupmessenger.model;

import lombok.Data;

@Data
public class Transaction {
    private String customerName;
    private String customerEmail;
    private String transactionID;
}
