package br.com.zup.zupmessenger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZupmessengerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZupmessengerApplication.class, args);
	}

}
