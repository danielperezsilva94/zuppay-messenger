package br.com.zup.zupmessenger.service;

import br.com.zup.zupmessenger.model.Transaction;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class EmailSenderService {
    private final String API_KEY = "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";
    private final String TEMPLATE_ID = "d-99adde2eae2045b18dbb9110b2fe31d0";
    private final String EMAIL_FROM = "zuppayment@gmail.com";
    private final String EMAIL_TITLE = "Payment Request";
    private final String API_URI = "mail/send";

    public void sendEmail(Transaction transaction) throws IOException {
        Mail mail = RequestBuilder(transaction);
        sendEmail(mail);
    }

    private  void sendEmail(Mail mail) throws IOException {
        SendGrid sg = new SendGrid(API_KEY);
        Request request = new Request();

        try {
            request.setMethod(Method.POST);
            request.setEndpoint(API_URI);
            request.setBody(mail.build());
            sg.api(request);

        } catch (
                IOException ex) {
            throw ex;
        }
    }

    private Mail RequestBuilder(Transaction transaction){
        Email from = new Email(EMAIL_FROM);
        Email to = new Email(transaction.getCustomerEmail());
        Content content = new Content("text/html", "1");

        Mail mail = new Mail(from, EMAIL_TITLE, to, content);
        mail.setTemplateId(TEMPLATE_ID);

        return mail;
    }
}
